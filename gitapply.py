import tempfile
import argparse
import hashlib
import struct
import zlib
import sys
import os

_gitchars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
_gitdec = {}
for i, c in enumerate(_gitchars): _gitdec[c] = i + 1

_b85chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!#$%&()*+-;<=>?@^_`{|}~"
_b85dec = {}
for i, c in enumerate(_b85chars): _b85dec[c] = i

_emptysha1 = "0000000000000000000000000000000000000000"

# decode a base85 string, implementation based on:
# https://github.com/git/git/blob/42817b96b1b80b56fd5a4d5e0d6239524b1832a3/base85.c#L40
def b85decode(text, outlen):
	c   = iter(text)
	out = []
	while outlen > 0:
		acc = _b85dec[c.next()]
		acc = acc * 85 + _b85dec[c.next()]
		acc = acc * 85 + _b85dec[c.next()]
		acc = acc * 85 + _b85dec[c.next()]
		acc = acc * 85 + _b85dec[c.next()]
		if acc > 4294967295: raise OverflowError("base85 overflow in hunk")
		out.append(acc)
		outlen -= 4
	out = struct.pack('>%dL' % len(out), *out)
	return out[:len(out) + outlen]

# Return the size of a given file object
def file_size(f):
	f.seek(0, 2)
	return f.tell()

# Copy l bytes from s to d
def file_copy(s, d, l):
	while l > 0:
		bl = min(l, 4096)
		l -= bl
		buf = s.read(bl)
		assert len(buf) == bl, "unable to read enough data"
		d.write(buf)

# Calculate the git sha1 of a given file object
def git_sha1(s):
	sha1 = hashlib.sha1()
	sha1.update("blob %d\0" % file_size(s))
	s.seek(0)
	while True:
		buf = s.read(4096)
		if buf == "": break # End of file
		sha1.update(buf)
	return sha1.hexdigest()

# based on https://github.com/git/git/blob/master/delta.h
def get_delta_hdr_size(delta):
	size = i = 0
	while True:
		cmd = struct.unpack('B', delta.read(1))[0]
		size |= (cmd & 0x7f) << i
		if not (cmd & 0x80): break
		i += 7
	return size

# based on https://github.com/git/git/blob/master/patch-delta.c
def patch_delta(delta, source, dest):
	delta.seek(0)

	# Read header (src_size, dst_size)
	src_size = get_delta_hdr_size(delta)
	if src_size != file_size(source):
		raise RuntimeError("source file has wrong size")
	dst_size = get_delta_hdr_size(delta)

	while True:
		buf = delta.read(1)
		if buf == "": break # End of file
		cmd = struct.unpack('B', buf)[0]

		if cmd & 0x80:
			cp_offs = cp_size = 0
			if cmd & 0x01: cp_offs  =  struct.unpack('B', delta.read(1))[0]
			if cmd & 0x02: cp_offs |= (struct.unpack('B', delta.read(1))[0] << 8)
			if cmd & 0x04: cp_offs |= (struct.unpack('B', delta.read(1))[0] << 16)
			if cmd & 0x08: cp_offs |= (struct.unpack('B', delta.read(1))[0] << 24)
			if cmd & 0x10: cp_size  =  struct.unpack('B', delta.read(1))[0]
			if cmd & 0x20: cp_size |= (struct.unpack('B', delta.read(1))[0] << 8)
			if cmd & 0x40: cp_size |= (struct.unpack('B', delta.read(1))[0] << 16)
			if cp_size == 0: cp_size = 0x10000
			assert cp_offs + cp_size <= src_size and cp_size <= dst_size, "decoded offset/size are out of range"
			source.seek(cp_offs)
			file_copy(source, dest, cp_size)
			dst_size -= cp_size

		elif cmd:
			assert cmd <= dst_size, "invalid command"
			buf = delta.read(cmd)
			assert len(buf) == cmd, "unable to read enough data"
			dest.write(buf)
			dst_size -= cmd

		else:
			raise RuntimeError("got unexpected delta opcode 0")

	# Sanity check
	assert dst_size == 0, "patch didn't produce output of expected size"

def get_temp_path(filename):
	root, base = os.path.split(filename)
	return tempfile.mkstemp(dir=root, prefix="%s-" % base)[1]

# Apply binary patches on a given file. Implementation based on:
# https://github.com/git/git/blob/7794a680e63a2a11b73cb1194653662f2769a792/builtin/apply.c#L4200
def apply_binary_patch(patch, dryrun=False):
	found_patch = False

	while True:
		line = patch.readline()
		if line == "": break
		line = line.rstrip("\n")

		# Search for git header, abort if we find a non-binary patch
		if line[0:4] == "@@ -" or line[0:4] == "+++ " or line[0:4] == "--- ":
			raise RuntimeError("no binary patch or fragment without header")
		if line[0:11] != "diff --git ": continue
		found_patch = True

		# Get oldname and newname
		try:
			patch_oldname, patch_newname = line[11:].split(" ")
		except ValueError:
			raise RuntimeError("unable to parse diff header")

		patch_oldsha1 	= None
		patch_newsha1 	= None
		patch_mode    	= None

		# Parse the header
		while True:
			line = patch.readline()
			if line == "": raise RuntimeError("unexpected end of file")
			line = line.rstrip("\n")

			if line[0:4] == "@@ -" or line[0:4] == "+++ " or line[0:4] == "--- ":
				raise NotImplementedError("only binary patches are supported")
			elif line[0:9] == "old mode " or line[0:18] == "deleted file mode ":
				pass # we ignore the previous mode
			elif line[0:9] == "new mode ":
				patch_mode = int(line[9:], 8) & 0777
			elif line[0:14] == "new file mode ":
				patch_mode = int(line[14:], 8) & 0777
			elif line[0:10] == "copy from " or line[0:8] == "copy to ":
				raise NotImplementedError("copy not supported yet")
			elif line[0:11] == "rename old " or line[0:11] == "rename new " or line[0:12] == "rename from " or line[0:10] == "rename to ":
				raise NotImplementedError("rename not supported yet")
			elif line[0:17] == "similarity index " or line[0:20] == "dissimilarity index ":
				pass # Ignore similarity / dissimilarity index
			elif line[0:6] == "index ":
				line = line[6:].split(" ")[0]
				patch_oldsha1, patch_newsha1 = line.split("..")
			elif line == "GIT binary patch":
				patch_type, patch_size = patch.readline().rstrip("\n").split(" ")
				assert patch_type in ["literal", "delta"], "unexpected patch type"
				patch_size 		= int(patch_size)
				patch_is_delta 	= (patch_type == "delta")
				break
			else:
				raise RuntimeError("unknown command '%s'" % line)

		# NOTE: This is a bit tricky, and only works with git - replace this with a more general algorithm later
		if patch_oldname[0:2] != "a/" or patch_newname[0:2] != "b/" or patch_oldname[2:] != patch_newname[2:]:
			raise NotImplementedError("oldname/newname doesn't look like a typical git path")

		patch_filename  = patch_oldname[2:]
		patch_is_new    = (patch_oldsha1 == _emptysha1 and not patch_is_delta and not os.path.exists(patch_filename))
		patch_is_delete = (patch_newsha1 == _emptysha1 and patch_size == 0 and not patch_is_delta and os.path.exists(patch_filename))

		# Open source file if its not a patch which creates a new file
		source = open(patch_filename, "rb") if not patch_is_new else None
		try:
			# If a source is given, then verify that it matches the oldsha1 sum
			if source and git_sha1(source) != patch_oldsha1:
				raise RuntimeError("patch doesn't apply, old sha1sum doesn't match")

			# File not deleted, we have to create a temporary file for the new content
			if not dryrun and not patch_is_delete:
				patch_tempfile = get_temp_path(patch_filename)
				try:

					with open(patch_tempfile, "w+b") as dest:
						delta = tempfile.TemporaryFile("w+b") if patch_is_delta else dest 
						try:
							# Load and decompress the contained data
							decompress = zlib.decompressobj()
							while True:
								line = patch.readline().rstrip("\n")
								if line == "": break
								if (len(line) - 1) % 5:
									raise RuntimeError("patch corrupted, line has invalid size")
								delta.write(decompress.decompress(b85decode(line[1:], _gitdec[line[0:1]])))
							delta.write(decompress.flush())
							delta.flush()
							# Ensure that the output has the right size
							if file_size(delta) != patch_size:
								raise RuntimeError("patch doesn't have the size specified in the header")
							# Apply the patch if its a delta
							if patch_is_delta: patch_delta(delta, source, dest)
						finally:
							if patch_is_delta: delta.close()
						# Verify the sha1 hash
						if git_sha1(dest) != patch_newsha1:
							raise RuntimeError("patch doesn't apply, new sha1sum doesn't match")

					# Set the file mode
					if patch_mode is not None: os.chmod(patch_tempfile, patch_mode)
					# Move the patch to its final place
					os.rename(patch_tempfile, patch_filename)
					patch_tempfile = None

				finally:
					if patch_tempfile: os.unlink(patch_tempfile)

		finally:
			if source: source.close()
		# Delete file
		if not dryrun and patch_is_delete: os.unlink(patch_filename)

	# Raise error if no patch was found, otherwise return exitcode 0
	if not found_patch:
		raise RuntimeError("no patch found in given file")
	return 0

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description="Python script to apply git binary diffs")

	parser.add_argument("-d", "--directory", metavar='dir', action='store', default=None,
						help="Change to the directory dir immediately after startup.")
	parser.add_argument("--dry-run", dest="dryrun", action='store_true', default=False,
						help="Don't modify the files, only check if the patch would apply.")
	parser.add_argument('patchfile', nargs='?', type=argparse.FileType('rb'), default=sys.stdin,
						help="File containing the patch or stdin if no argument is given")

	args = parser.parse_args()

	if args.directory:
		os.chdir(args.directory)

	sys.exit(apply_binary_patch(args.patchfile, dryrun=args.dryrun))